# Web IDE Scrolling Bug

This project is designed to illustrate a bug where the GitLab Web IDE does not maintain
file position when switching between editor tabs in the MR view.

